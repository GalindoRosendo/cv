# CV

## Personal Data

Rosendo Guadalupe Galindo
López

Rosendo.galindo
@expeditors.com

rosendo_galindo
@hotmail.com

GalindoRosendo

GalindoRosendo

Edad 27 años

Dirección
Av. Jesús Guajardo #2125

Colonia
Américo Villareal
Guerra

Ciudad
Nuevo Laredo, Tamps.
Mexico.

Teléfono
867 7244354

Celular
867 1413415

## Work Experience

Diciembre 2016 – Actualidad
Expeditors Internacional de Mexico S.A. de C.V.
Analista de Sistemas y Lider de Programacion

Julio 2016 - diciembre 2016
Grupo Dival S.A. de C.V
Analista de Sistemas y Programador

Febrero 2016-Julio 2016
Servicios de Tecnología Informática
Programación de Aplicaciones Multiplataforma, Administración de Bases de Datos, Servidores e Infraestructuras de Red y
Soporte Técnico

Septiembre 2014-Enero 2016
Multidivisas Orión Centro Cambiario S.A. de C.V.
Programador Web, Programación de Aplicaciones .NET, Administración de Bases de Datos y Soporte Técnico

## Estudios

Agosto 2011- junio 2016
Instituto Tecnológico de Nuevo Laredo
Ingeniería en Sistemas Computacionales con Especialidad en
Programación Móvil

## Referencias Personales

Ing. Luis de los Santos.

Ing. Francisco Reyes Madrigal.

Ing. Ludwing Daniel Oliva Perea.

## Otros datos

### Idioma

    Inglés.
    Curso tomado en el Instituto Tecnológico de Nuevo Laredo.

### Lenguajes de Programación

C#, Java, PHP, JavaScript, Python.
Gestores de Bases de Datos:
MySQL, SQL Server, MongoDB, MariaDB.

### Frameworks

CodeIgniter, Boostrap, Bottle, Express.
Sistemas Operativos:
Windows (MS-DOS), Linux, Mac OS.

### Plataformas Móviles

Android, Windows Phone, IOS.

### Cursos

Patrones de diseño y algoritmos (Platzi)
MongoDB para desarrolladores NodeJS (MondoDB University)
MongoDB para desarrolladores Python (MondoDB University)
Taller de Github y Git. (ITNL)
Curso de Programación para Android. (ITNL)
Taller avanzado de C#. (ITNL)
Mantenimiento de monitores e impresoras. (ITNL)
